﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modular.Animation
{
    public sealed class SpriteFrame : FrameSpatialInfo
    {
        public SpriteFrame()
        {

        }

        public int Folder { get; set; }

        public int File { get; set; }
        
        public bool Clockwise { get; set; }
        
        public int ZOrder { get; set; }

        public int? Parent { get; set; }

        /// <summary>
        /// precalculated
        /// </summary>
        public AnimationTransform Transform { get; set; }
    }
}
