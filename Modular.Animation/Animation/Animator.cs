﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if NETFX_CORE
using WhiteWolf.Engine.Core;
using WhiteWolf.Engine.Graphics;
#endif

namespace Modular.Animation
{
    public sealed class Animator
    {
        #region Private Members

        private struct RenderedPosition
        {
            public Vector2 Position;
            public Vector2 Pivot;
            public float Angle;
            public Vector2 Scale;
            public float Alpha;
            public SpriteEffects Effects;
        }

        private float _fps;
        private Animation _current;
        private double _elapsedTime;
        private Entity _entity;
        private IDictionary<int, IDictionary<int, Texture2D>> _materials;
        private int _frameIndex;
        #endregion

        #region CTOR
        internal Animator(Entity entity, IDictionary<int, IDictionary<int, Texture2D>> materials, float fps)
        {
            _entity = entity;
            _materials = materials;
            _current = entity[0];
            _fps = fps;

            Playing = true;
        }
        #endregion

        #region Methods
        public void SetFrame(int frame)
        {
            _frameIndex = (int)MathHelper.Clamp(frame, 0, _current.Frames.Count - 1);
        }

        public void ResetAnimation()
        {
            _elapsedTime = 0;
            _frameIndex = 0;
        }

        public void ChangeAnimation(int id)
        {
            if (_entity.ContainsKey(id))
            {
                _current = _entity[id];
                ResetAnimation();
            }
        }

        public void ChangeAnimation(string name)
        {
            var anim = _entity.Where(s => string.Equals(s.Value.Name, name)).FirstOrDefault().Key;

            ChangeAnimation(anim);
        }

        public void ApplyCharacterMap(int id)
        {
            var map = _entity.CharacterMaps.Where(s => s.Id == id).FirstOrDefault();

            if (map != null)
            {
                foreach (var item in map)
                {
                    _materials[item.FolderId][item.FileId] = _materials[item.TargetFolderId][item.TargetFileId];
                }
            }
        }

        public void ApplyCharacterMap(string name)
        {
            var map = _entity.CharacterMaps.Where(s => string.Equals(s.Name, name)).FirstOrDefault();

            if (map != null)
            {
                foreach (var item in map)
                {
                    _materials[item.FolderId][item.FileId] = _materials[item.TargetFolderId][item.TargetFileId];
                }
            }
        }

        /// <summary>
        /// get animation by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Animation</returns>
        public Animation GetAnimation(int id)
        {
            if (_entity.ContainsKey(id))
            {
                return _entity[id];
            }

            return null;
        }

        /// <summary>
        ///  get animation by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Animation</returns>
        public Animation GetAnimation(string name)
        {
            var anim = _entity.Where(s => string.Equals(s.Value.Name, name)).FirstOrDefault().Key;

            return GetAnimation(anim);
        }

        public void Update(GameTime gameTime)
        {
            if (Playing)
            {
                _elapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
                if (_elapsedTime > _current.Length)
                {
                    if (_current.Loop)
                    {
                        _elapsedTime %= _current.Length;//loop
                    }
                    else
                    {
                        _elapsedTime = _current.Length;
                    }
                }

                _frameIndex = (int)Math.Ceiling((_elapsedTime / 1000.0) * _fps) - 1;
            }
        }

        //public void AdvanceFrame()
        //{
        //    _frameIndex++;
        //    _frameIndex %= _current.Frames.Count;
        //}

        /// <summary>
        /// call after Update
        /// </summary>
        public void PlaySound()
        {
            Frame frame = _current.Frames[_frameIndex];

            foreach (var sound in frame.SoundEffectInstances)
            {
                sound.Play();
            }

        }

        /// <summary>
        /// call after each update
        /// </summary>
        public IList<RenderedBox> GetBoxes()
        {
            Frame frame = _current.Frames[_frameIndex];
            IList<RenderedBox> list = new List<RenderedBox>();

            foreach (var box in frame.BoxFrames)
            {
                RenderedPosition pos = GetRenderedPosition(box.Transform);

                list.Add(new RenderedBox()
                    {
                        Angle = pos.Angle,
                        Position = pos.Position,
                        Scale = pos.Scale
                    });
            }

            return list;
        }

        /// <summary>
        /// call after each update call
        /// </summary>
        /// <returns></returns>
        public IList<Vector2> GetPoints()
        {
            Frame frame = _current.Frames[_frameIndex];
            IList<Vector2> list = new List<Vector2>();

            foreach (var point in frame.PointFrames)
            {
                Vector2 pos = new Vector2(Position.X + (point.Transform.Position.X * (FlipX ? -1 : 1)),
                    Position.Y + (point.Transform.Position.Y * (FlipY ? -1 : 1)));

                list.Add(pos);
            }

            return list;
        }

        private RenderedPosition GetRenderedPosition(SpriteFrame fimg)
        {
            // Apply transforms
            AnimationTransform transform = fimg.Transform;
            RenderedPosition result = new RenderedPosition();

            result.Alpha = transform.Alpha;

            result.Position.Y = Position.Y + (transform.Position.Y * (FlipY ? -1 : 1));
            result.Position.X = Position.X + (transform.Position.X * (FlipX ? -1 : 1));

            bool flipX = FlipX;
            bool flipY = FlipY;

            result.Angle = transform.Angle;
            if (flipX != flipY)
            {
                result.Angle *= -1;
            }

            result.Pivot = fimg.Pivot;
            if (flipX)
            {
                result.Pivot.X = _materials[fimg.Folder][fimg.File].Width - result.Pivot.X;
            }

            if (flipY)
            {
                result.Pivot.Y = _materials[fimg.Folder][fimg.File].Height - result.Pivot.Y;
            }

            result.Scale = transform.Scale;
            if (result.Scale.X < 0)
            {
                result.Scale.X = -result.Scale.X;
                flipX = !flipX;
            }
            if (result.Scale.Y < 0)
            {
                result.Scale.Y = -result.Scale.Y;
                flipY = !flipY;
            }

            result.Effects = SpriteEffects.None;
            if (flipX)
            {
                result.Effects |= SpriteEffects.FlipHorizontally;
            }
            if (flipY)
            {
                result.Effects |= SpriteEffects.FlipVertically;
            }

            return result;
        }

        private RenderedPosition GetRenderedPosition(AnimationTransform transform)
        {
            // Apply transforms
            RenderedPosition result = new RenderedPosition();

            result.Alpha = transform.Alpha;

            result.Position.Y = Position.Y + (transform.Position.Y * (FlipY ? -1 : 1));
            result.Position.X = Position.X + (transform.Position.X * (FlipX ? -1 : 1));

            bool flipX = FlipX;
            bool flipY = FlipY;

            result.Angle = transform.Angle;
            if (flipX != flipY)
            {
                result.Angle *= -1;
            }

            result.Scale = transform.Scale;
            if (result.Scale.X < 0)
            {
                result.Scale.X = -result.Scale.X;
                flipX = !flipX;
            }
            if (result.Scale.Y < 0)
            {
                result.Scale.Y = -result.Scale.Y;
                flipY = !flipY;
            }

            return result;
        }

#if NETFX_CORE
        public void Draw(RenderHelper renderHelper)
        {
            Frame frame = _current.Frames[_frameIndex];

            float layer = 0;
            foreach (SpriteFrame ki in frame.SpriteFrames)
            {
                RenderedPosition pos = GetRenderedPosition(ki);

                //we can have image alpha and bone alpha
                //bone alpha will apply to all images on that bone
                renderHelper.RenderTexture(_materials[ki.Folder][ki.File], pos.Position, null, Color.White * pos.Alpha,
                                 pos.Angle, pos.Pivot, pos.Scale, pos.Effects, layer);

                layer += 0.01f;
            }
        }
#endif

#if WINDOWS
        public void Draw(SpriteBatch spriteBatch)
        {
            Frame frame = _current.Frames[_frameIndex];

            float layer = 0;
            foreach (SpriteFrame ki in frame.SpriteFrames)
            {
                RenderedPosition pos = GetRenderedPosition(ki);

                //we can have image alpha and bone alpha
                //bone alpha will apply to all images on that bone
                spriteBatch.Draw(_materials[ki.Folder][ki.File], pos.Position, null, Color.White * pos.Alpha,
                                 pos.Angle, pos.Pivot, pos.Scale, pos.Effects, layer);

                layer += 0.01f;
            }

        }
#endif


        #endregion

        #region Properties

        /// <summary>
        /// position of the animation
        /// </summary>
        public Vector2 Position { get; set; }

        /// <summary>
        /// flip the animation on the x axis
        /// </summary>
        public bool FlipX { get; set; }

        /// <summary>
        /// flip the animation on the y axis
        /// </summary>
        public bool FlipY { get; set; }

        /// <summary>
        /// stop the animation
        /// </summary>
        public bool Playing { get; set; }


        #endregion


    }
}
