﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modular.Animation
{
    public struct RenderedBox
    {
        public Vector2 Position { get; set; }

        public float Angle { get; set; }

        public Vector2 Scale { get; set; }
    }
}
