﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modular.Animation
{

    internal sealed class Frame
    {
        public Frame()
        {

        }

        public AnimationTransform ApplyTransform(AnimationTransform baseTransform, Vector2 scale, float angle, Vector2 position, float alpha)
        {
            Matrix m = Matrix.CreateScale(baseTransform.Scale.X, baseTransform.Scale.Y, 0) *
                        Matrix.CreateRotationZ(baseTransform.Angle) *
                        Matrix.CreateTranslation(baseTransform.Position.X, baseTransform.Position.Y, 0);

            AnimationTransform result = new AnimationTransform();
            result.Scale = baseTransform.Scale * scale;
            result.Angle = baseTransform.Angle + angle;
            result.Position = Vector2.Transform(position, m);
            result.Alpha = baseTransform.Alpha * alpha;

            return result;
        }

        private AnimationTransform ApplyBoneTransforms(BoneFrame bone, Vector2 scale, Vector2 offset)
        {
            AnimationTransform baseTransform = (bone.Parent != null)
                                                  ? ApplyBoneTransforms(BoneFrames[bone.Parent.Value], scale, offset)
                                                  : new AnimationTransform(0, offset, scale, 1f);

            // Apply transforms from self and/or parent
            return ApplyTransform(baseTransform, bone.Scale, bone.Angle, bone.Position, bone.Alpha);
        }

        /// <summary>
        /// applies transforms
        /// </summary>
        /// <param name="fimg">FrameImage to apply transform</param>
        /// <param name="scale">scale the entire animation</param>
        /// <param name="offset">offset the animation</param>
        /// <returns></returns>
        public AnimationTransform ApplyBoneTransforms(SpriteFrame fimg, Vector2 scale, Vector2 offset)
        {
            // Apply transforms from self and/or parent
            AnimationTransform baseTransform = (fimg.Parent != null)
                                                   ? ApplyBoneTransforms(BoneFrames[fimg.Parent.Value], scale, offset)
                                                   : new AnimationTransform(0, offset, scale, 1f);

            return ApplyTransform(baseTransform, fimg.Scale, fimg.Angle, fimg.Position, fimg.Alpha);
        }

        public IEnumerable<SpriteFrame> SpriteFrames { get; set; }

        public IList<BoneFrame> BoneFrames { get; set; }

        public IList<BoxFrame> BoxFrames { get; set; }

        public IList<PointFrame> PointFrames { get; set; }

        public IList<SoundEffectInstance> SoundEffectInstances { get; set; }
    }
}
