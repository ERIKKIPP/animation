﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modular.Animation
{
    /// <summary>
    /// 
    /// </summary>
    public struct AnimationTransform
    {
        public Vector2 Position;
        public float Angle;
        public Vector2 Scale;
        public float Alpha;

        public AnimationTransform(float angle, Vector2 position, Vector2 scale, float alpha)
        {
            Angle = angle;
            Position = position;
            Scale = scale;
            Alpha = alpha;
        }
    }
}
