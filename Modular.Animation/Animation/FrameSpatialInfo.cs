﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modular.Animation
{
    public abstract class FrameSpatialInfo
    {
        public FrameSpatialInfo()
        {

        }

        public Vector2 Position { get; set; }

        public float Angle { get; set; }

        public Vector2 Scale { get; set; }
        
        public float Alpha { get; set; }

        public Vector2 Pivot { get; set; }
    }
}
