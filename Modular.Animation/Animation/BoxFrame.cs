﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modular.Animation
{
    internal sealed class BoxFrame : FrameSpatialInfo
    {
        public BoxFrame()
        {

        }

        /// <summary>
        /// precalculated
        /// </summary>
        public AnimationTransform Transform { get; set; }
    }
}
