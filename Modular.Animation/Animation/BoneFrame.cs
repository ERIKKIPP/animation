﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modular.Animation
{
    internal sealed class BoneFrame : FrameSpatialInfo
    {
        public BoneFrame()
        {

        }
                
        public int? Parent { get; set; }

        public int Folder { get; set; }

        public int File { get; set; }

    }
}
