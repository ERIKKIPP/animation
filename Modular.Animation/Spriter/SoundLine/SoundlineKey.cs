﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Modular.Animation
{
    internal sealed class SoundlineKey
    {
        public SoundlineKey(XElement ele)
        {
            KeyObjects = new List<SoundlineKeyObject>();
           
            Id = AnimationData.GetValue<int>(ele.Attribute("id"));
            Time = AnimationData.GetValue<float>(ele.Attribute("time"));

            foreach (var row in ele.Elements())
            {
                SoundlineKeyObject obj = new SoundlineKeyObject(row);

                KeyObjects.Add(obj);
            }
        }

        public IList<SoundlineKeyObject> KeyObjects { get; set; }

        public int Id { get; private set; }

        public float Time { get; set; }

    }
}
