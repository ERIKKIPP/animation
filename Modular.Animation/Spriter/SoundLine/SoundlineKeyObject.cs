﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Modular.Animation
{
    internal sealed class SoundlineKeyObject
    {
        public SoundlineKeyObject(XElement ele)
        {
            FolderId = AnimationData.GetValue<int>(ele.Attribute("folder"));
            FileId = AnimationData.GetValue<int>(ele.Attribute("file"));
        }

        public int FolderId { get; private set; }

        public int FileId { get; private set; }
    }
}
