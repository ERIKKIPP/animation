﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Modular.Animation
{
    internal sealed class CharacterMap : List<Map>
    {
        public CharacterMap(XElement ele)
        {
            Id = AnimationData.GetValue<int>(ele.Attribute("id"));
            Name = AnimationData.GetValue<string>(ele.Attribute("name"));

            foreach (var row in ele.Elements())
            {
                this.Add(new Map(row));
            }
        }

        public int Id { get; private set; }

        public string Name { get; private set; }
    }
}
