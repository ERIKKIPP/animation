﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Modular.Animation
{
    /// <summary>
    ///  <entity id="0" name="monster">
    ///   <animation id="0" name="Idle" length="2000">
    ///    <timeline id="0" name="shadow">
    ///            <key id="0" spin="0">
    ///             <object folder="0" file="0" x="-1" pivot_x="0.510903" pivot_y="0.5625"/>
    /// </summary>
    internal sealed class TimeLineKeySpriteObject : SpatialTimelineKey
    {
        public TimeLineKeySpriteObject(XElement ele)
            : base(ele)
        {
            var row = ele.Elements().FirstOrDefault();
            MaterialFolderId = AnimationData.GetValue<int>(row.Attribute("folder"));
            MaterialFileId = AnimationData.GetValue<int>(row.Attribute("file"));

            MaterialImageFile file = AnimationData.Folders[MaterialFolderId][MaterialFileId] as MaterialImageFile;

            float x = AnimationData.GetValue<float>(row.Attribute("pivot_x"));
            float y = AnimationData.GetValue<float>(row.Attribute("pivot_y"));
            //use timeline if it's there else use the file
            SpatialInfo.Pivot = new Vector2(x > 0 ? x * file.Width : file.PivotX * file.Width,
             y > 0 ? (1.0f - y) * file.Height : (1.0f - file.PivotY) * file.Height);//flip Y            
        }

        public int MaterialFolderId { get; private set; }

        public int MaterialFileId { get; private set; }

    }
}
