﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Modular.Animation
{
    /// <summary>
    ///  <entity id="0" name="monster">
    ///   <animation id="0" name="Idle" length="2000">
    ///    <timeline id="0" name="shadow">
    ///            <key id="0" spin="0">
    /// </summary>
    internal abstract class TimeLineKey 
    {
        public TimeLineKey(XElement ele)
        {
            Id = AnimationData.GetValue<int>(ele.Attribute("id"));
            Spin = AnimationData.GetValue<int>(ele.Attribute("spin")) == -1 ? true : false;
            Time = AnimationData.GetValue<float>(ele.Attribute("time"));            
        }

        public int Id { get; private set; }

        /// <summary>
        /// 1 means the object should spin counter-clockwise when tweened
        /// and -1 means it should spin clockwise
        /// true = spin counter-clockwise when tweened
        /// false = spin clockwise
        /// </summary>
        public bool Spin { get; private set; }

        public float Time { get; private set; }
    }
}
