﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Modular.Animation
{
    internal abstract class SpatialTimelineKey : TimeLineKey
    {
        public SpatialTimelineKey(XElement ele)
            : base(ele)
        {
            SpatialInfo = new SpatialInfo();
            var row = ele.Elements().FirstOrDefault();
            
            float x = row.Attribute("scale_x") == null ? 1 : AnimationData.GetValue<float>(row.Attribute("scale_x"));
            float y = row.Attribute("scale_y") == null ? 1 : AnimationData.GetValue<float>(row.Attribute("scale_y"));
            SpatialInfo.Scale = new Vector2(x, y);

            SpatialInfo.Angle = -MathHelper.ToRadians(AnimationData.GetValue<float>(row.Attribute("angle")));

            //flip Y
            SpatialInfo.Position = new Vector2(AnimationData.GetValue<float>(row.Attribute("x")), -AnimationData.GetValue<float>(row.Attribute("y")));

            SpatialInfo.Alpha = row.Attribute("a") == null ? 1 : AnimationData.GetValue<float>(row.Attribute("a"));
        }

        public SpatialInfo SpatialInfo { get; set; }
    }
}
