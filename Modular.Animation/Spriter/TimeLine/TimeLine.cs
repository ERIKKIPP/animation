﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Modular.Animation
{
    /// <summary>
    /// <entity id="0" name="monster">
    /// <animation id="0" name="Idle" length="2000">
    /// <timeline id="0" name="shadow">
    /// </summary>
    internal sealed class TimeLine : Dictionary<int, TimeLineKey>
    {
        public TimeLine(XElement ele)
        {
            Id = AnimationData.GetValue<int>(ele.Attribute("id"));
            Name = AnimationData.GetValue<string>(ele.Attribute("name"));

            string type = AnimationData.GetValue<string>(ele.Attribute("object_type"));

            if (!string.IsNullOrEmpty(type))
            {
                TimeLineType timeLineType = TimeLineType.Sprite;
                if (Enum.TryParse(type, true, out timeLineType))
                {
                    TimeLineType = timeLineType;
                }
            }

            foreach (var row in ele.Elements())
            {
                TimeLineKey obj = null;

                switch (TimeLineType)
                {
                    case TimeLineType.Sprite:
                        obj = new TimeLineKeySpriteObject(row);
                        break;
                    case TimeLineType.Bone:
                        obj = new TimeLineKeyBoneObject(row);
                        break;
                    case TimeLineType.Box:
                        obj = new TimeLineKeyBoxObject(row);
                        break;
                    case TimeLineType.Point:
                        obj = new TimeLineKeyPointObject(row);
                        break;
                    case TimeLineType.Sound:
                        break;
                    case TimeLineType.Entity:
                        break;
                    case TimeLineType.Variable:
                        break;
                    default:
                        break;
                }                 

                this[obj.Id] = obj;
            }
        }

        public int Id { get; private set; }

        public string Name { get; private set; }

        public TimeLineType TimeLineType { get; set; }
    }
}
