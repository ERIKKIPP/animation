﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modular.Animation
{
    internal enum TimeLineType
    {
        Sprite, Bone, Box, Point, Sound, Entity, Variable
    }
}
