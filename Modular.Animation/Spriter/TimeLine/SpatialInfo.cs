﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modular.Animation
{
    public sealed class SpatialInfo
    {
        public SpatialInfo()
        {

        }

        public Vector2 Position { get; set; }

        public Vector2 Pivot { get; set; }

        public float Angle { get; set; }

        public float Alpha { get; set; }

        public Vector2 Scale { get; set; }

        /// <summary>
        /// 1 means the object should spin counter-clockwise when tweened
        /// and -1 means it should spin clockwise
        /// true = spin counter-clockwise when tweened
        /// false = spin clockwise
        /// </summary>
        public bool Spin { get; set; }
    }
}
