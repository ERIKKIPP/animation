﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Modular.Animation
{
    internal abstract class MaterialFile
    {
        public MaterialFile(XElement ele)
        {
            Id = AnimationData.GetValue<int>(ele.Attribute("id"));
            Name = AnimationData.GetValue<string>(ele.Attribute("name"));
           
        }

        public int Id { get; private set; }

        public string Name { get; private set; }
            
    }
}
