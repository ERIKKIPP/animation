﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Modular.Animation
{
    /// <summary>
    /// <mainline>
    ///            <key id="0">
    ///                <bone_ref id="0" timeline="23" key="0"/>
    /// </summary>
    internal sealed class MainLineKeyBoneObjectRef : ObjectReference
    {
        public MainLineKeyBoneObjectRef()
        {

        }

        public MainLineKeyBoneObjectRef(XElement ele)
        {
            Id = AnimationData.GetValue<int>(ele.Attribute("id"));
            TimeLine = AnimationData.GetValue<int>(ele.Attribute("timeline"));
            Key = AnimationData.GetValue<int>(ele.Attribute("key"));

            if (ele.Attribute("parent") != null)
            {
                Parent = AnimationData.GetValue<int>(ele.Attribute("parent"));
            }
        }

        public int Id { get; private set; }
        
        
    }
}
