﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modular.Animation
{
    internal abstract class ObjectReference
    {
        public ObjectReference()
        {

        }

        /// <summary>
        /// this is a reference to the timeline id
        /// </summary>
        public int TimeLine { get; set; }

        /// <summary>
        /// reference to the TimeLineKey in the referenced TimeLine
        /// </summary>
        public int Key { get; set; }

        /// <summary>
        /// -1==no parent
        /// </summary>
        public int? Parent { get; set; }
    }
}
