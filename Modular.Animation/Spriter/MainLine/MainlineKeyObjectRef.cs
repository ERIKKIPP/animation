﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Modular.Animation
{
    /// <summary>
    /// <entity id="0" name="monster">
    /// <mainline>
    ///  <key id="0">
    ///    <object_ref id="0" name="shadow" folder="0" file="0" abs_x="-1" abs_y="0" abs_pivot_x="0.510903" abs_pivot_y="0.5625" 
    ///    abs_angle="0" abs_scale_x="1" abs_scale_y="1" abs_a="1" timeline="0" key="0" z_index="0"/>
    ///    transient objects
    /// </summary>
    internal sealed class MainlineKeyObjectRef : ObjectReference
    {
        public MainlineKeyObjectRef()
        {

        }

        public MainlineKeyObjectRef(XElement ele)
        {

            Id = AnimationData.GetValue<int>(ele.Attribute("id"));
            Name = AnimationData.GetValue<string>(ele.Attribute("name"));
            TimeLine = AnimationData.GetValue<int>(ele.Attribute("timeline"));
            Key = AnimationData.GetValue<int>(ele.Attribute("key"));
            MaterialFolderId = AnimationData.GetValue<int>(ele.Attribute("folder"));
            MaterialFileId = AnimationData.GetValue<int>(ele.Attribute("file"));

            if (ele.Attribute("parent") != null)
            {
                Parent = AnimationData.GetValue<int>(ele.Attribute("parent"));
            }

            Pivot = new Vector2(AnimationData.GetValue<float>(ele.Attribute("abs_pivot_x")), -AnimationData.GetValue<float>(ele.Attribute("abs_pivot_y")));

            Angle = -MathHelper.ToRadians(AnimationData.GetValue<float>(ele.Attribute("abs_angle")));
            Alpha = AnimationData.GetValue<float>(ele.Attribute("abs_a"));
            ZOrder = AnimationData.GetValue<int>(ele.Attribute("z_index"));

            Position = new Vector2(AnimationData.GetValue<float>(ele.Attribute("abs_x")), -AnimationData.GetValue<float>(ele.Attribute("abs_y")));
            Scale = new Vector2(AnimationData.GetValue<float>(ele.Attribute("abs_scale_x")), AnimationData.GetValue<float>(ele.Attribute("abs_scale_y")));
        }

        public int Id { get; private set; }

        public string Name { get; private set; }

        public int MaterialFolderId { get; private set; }

        public int MaterialFileId { get; private set; }

        public Vector2 Position { get; private set; }

        /// <summary>
        /// not sure if I really need this
        /// </summary>
        public Vector2 Pivot { get; private set; }

        public float Angle { get; private set; }

        public Vector2 Scale { get; private set; }

        public float Alpha { get; private set; }

        /// <summary>
        /// The 'z_index' indicates the order in which the object should be drawn, but 
        /// this information can also be inferred from the order in which the object or 
        /// object_ref appears in the key.
        /// </summary>
        public int ZOrder { get; private set; }
    }
}
