﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Modular.Animation
{
    /// <summary>
    /// <entity id="0" name="monster">
    /// <mainline>
    ///  <key id="0">
    /// </summary>
    internal sealed class MainlineKey
    {
        public MainlineKey(XElement ele)
        {
            KeyObjectRefs = new List<MainlineKeyObjectRef>();
            BoneObjectRefs = new List<MainLineKeyBoneObjectRef>();

            Id = AnimationData.GetValue<int>(ele.Attribute("id"));
            Time = AnimationData.GetValue<float>(ele.Attribute("time"));

            foreach (var row in ele.Elements().Where(s => string.Equals(s.Name.ToString(), "object_ref", StringComparison.CurrentCultureIgnoreCase)))
            {
                MainlineKeyObjectRef obj = new MainlineKeyObjectRef(row);

                KeyObjectRefs.Add(obj);
            }

            foreach (var row in ele.Elements().Where(s => string.Equals(s.Name.ToString(), "bone_ref", StringComparison.CurrentCultureIgnoreCase)))
            {
                MainLineKeyBoneObjectRef obj = new MainLineKeyBoneObjectRef(row);

                BoneObjectRefs.Add(obj);
            }
        }

        public IList<MainlineKeyObjectRef> KeyObjectRefs { get; set; }

        public IList<MainLineKeyBoneObjectRef> BoneObjectRefs { get; set; }

        public int Id { get; private set; }

        public float Time { get; set; }

    }
}
