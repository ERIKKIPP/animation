﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Modular.Animation
{
    /// <summary>
    ///  <entity id="0" name="monster">
    ///  <animation id="0" name="Idle" length="2000">
    /// </summary>
    public sealed class Animation
    {
        internal Animation(XElement ele)
        {
            Frames = new List<Frame>();
            Mainline = new List<MainlineKey>();
            TimeLines = new Dictionary<int, TimeLine>();
            Soundline = new List<SoundlineKey>();

            Id = AnimationData.GetValue<int>(ele.Attribute("id"));
            Name = AnimationData.GetValue<string>(ele.Attribute("name"));
            Length = AnimationData.GetValue<int>(ele.Attribute("length"));

            if (ele.Attribute("looping") == null)
            {
                Loop = true;
            }
            else
            {
                Loop = AnimationData.GetValue<bool>(ele.Attribute("looping"));
            }

            foreach (var row in ele.Elements("mainline").Elements())
            {
                Mainline.Add(new MainlineKey(row));
            }

            foreach (var row in ele.Elements("timeline"))
            {
                TimeLine obj = new TimeLine(row);

                TimeLines[obj.Id] = obj;
            }

            foreach (var row in ele.Elements("soundline").Elements())
            {
                Soundline.Add(new SoundlineKey(row));
            }
        }

        internal MainlineKey GetCurrentMainlineKeyFromTime(float frameTime)
        {
            //get a frame <key id="0">
            return (from kf in Mainline
                    where kf.Time <= frameTime
                    orderby kf.Time descending
                    select kf).FirstOrDefault();
        }

        internal MainlineKey GetNextMainLineKeyFromTime(float frameTime)
        {
            //get next frame <key id="1" time="49">
            return (from kf in Mainline
                    where kf.Time > frameTime
                    orderby kf.Time ascending
                    select kf).FirstOrDefault();
        }

        internal SoundlineKey GetCurrentSoundlineKeyFromTime(float beginframeTime, float endframeTime)
        {
            //get a frame <key id="0">
            return (from kf in Soundline
                    where kf.Time >= beginframeTime && kf.Time <= endframeTime
                    orderby kf.Time descending
                    select kf).FirstOrDefault();
        }
        
        public int Id { get; private set; }

        public string Name { get; private set; }

        /// <summary>
        /// milliseconds
        /// </summary>
        public int Length { get; private set; }

        internal IList<MainlineKey> Mainline { get; private set; }

        internal IDictionary<int, TimeLine> TimeLines { get; private set; }

        internal IList<SoundlineKey> Soundline { get; private set; }

        internal IList<Frame> Frames { get; set; }

        public bool Loop { get; private set; }
    }
}
