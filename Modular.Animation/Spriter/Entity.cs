﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Modular.Animation
{
    internal sealed class Entity : Dictionary<int, Animation>
    {
        public Entity(XElement ele)
        {
            CharacterMaps = new List<CharacterMap>();
            Id = AnimationData.GetValue<int>(ele.Attribute("id"));
            Name = AnimationData.GetValue<string>(ele.Attribute("name"));

            foreach (var row in ele.Elements("animation"))
            {
                Animation obj = new Animation(row);

                this[obj.Id] = obj;
            }

            foreach (var row in ele.Elements("character_map"))
            {
                CharacterMap obj = new CharacterMap(row);

                CharacterMaps.Add(obj);
            }
        }

        public int Id { get; private set; }

        public string Name { get; private set; }

        public IList<CharacterMap> CharacterMaps { get; private set; }
    }
}
