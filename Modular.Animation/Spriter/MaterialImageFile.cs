﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Modular.Animation
{
    internal sealed class MaterialImageFile : MaterialFile
    {
        public MaterialImageFile(XElement ele)
            : base(ele)
        {
            Width = AnimationData.GetValue<int>(ele.Attribute("width"));
            Height = AnimationData.GetValue<int>(ele.Attribute("height"));
            PivotX = AnimationData.GetValue<float>(ele.Attribute("pivot_x"));
            PivotY = AnimationData.GetValue<float>(ele.Attribute("pivot_y")); 
        }

        public int Width { get; private set; }

        public int Height { get; private set; }

        public float PivotX { get; private set; }

        public float PivotY { get; private set; }
    }
}
