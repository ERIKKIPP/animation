﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Modular.Animation
{
    /// <summary>
    /// loads the spriter project and created frames
    /// </summary>
    public sealed class AnimationData
    {
        private IDictionary<int, IDictionary<int, Texture2D>> _materials;

        private IDictionary<int, IDictionary<int, SoundEffect>> _sounds;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="content"></param>
        public AnimationData(XDocument doc, ContentManager content)
        {
            Entities = new List<Entity>();

            var folders = (from c in doc.Descendants("spriter_data").Elements("folder")
                           select c);

            _materials = new Dictionary<int, IDictionary<int, Texture2D>>();
            _sounds = new Dictionary<int, IDictionary<int, SoundEffect>>();

            Folders = new Dictionary<int, MaterialFolder>();

            foreach (var row in folders)
            {
                MaterialFolder obj = new MaterialFolder(row);

                Folders[obj.Id] = obj;

                //images
                Dictionary<int, Texture2D> imageList = null;
                foreach (var item in obj.Values.OfType<MaterialImageFile>())
                {
#if NETFX_CORE || WINDOWS

                    if (imageList == null)
                    {
                        imageList = new Dictionary<int, Texture2D>();
                    }

                    try
                    {
                        imageList[item.Id] = content.Load<Texture2D>(item.Name);

                    }
                    catch (ContentLoadException) { }
                    catch (Exception) { }
#endif
                }

                if (imageList != null && imageList.Count > 0)
                {
                    _materials[obj.Id] = imageList;
                }

                //sounds
                Dictionary<int, SoundEffect> soundlist = null;
                foreach (var item in obj.Values.OfType<MaterialSoundFile>())
                {
#if NETFX_CORE || WINDOWS

                    if (soundlist == null)
                    {
                        soundlist = new Dictionary<int, SoundEffect>();
                    }

                    try
                    {
                        soundlist[item.Id] = content.Load<SoundEffect>(item.Name);

                    }
                    catch (ContentLoadException) { }
                    catch (Exception) { }
#endif
                }

                if (soundlist != null && soundlist.Count > 0)
                {
                    _sounds[obj.Id] = soundlist;
                }

            }

            var entities = (from c in doc.Descendants("spriter_data").Elements("entity")
                            select c);

            foreach (var row in entities)
            {
                Entity obj = new Entity(row);


                Entities.Add(obj);
            }

        }

        internal static IDictionary<int, MaterialFolder> Folders { get; private set; }

        internal static T GetValue<T>(XAttribute att)
        {
            T value = default(T);

            if (att != null)
            {
                new SwitchOnType<T>()
                  .Case<int>(() =>
                  {
                      int x = 0;
                      if (int.TryParse(att.Value, out x))
                      {
                          value = (T)Convert.ChangeType(x, typeof(T));
                      }
                  })
                  .Case<decimal>(() =>
                  {
                      decimal x = 0;
                      if (decimal.TryParse(att.Value, NumberStyles.Float, CultureInfo.CurrentCulture, out x))
                      {
                          value = (T)Convert.ChangeType(x, typeof(T));
                      }
                  })
                  .Case<string>(() =>
                  {
                      value = (T)Convert.ChangeType(att.Value, typeof(T));
                  })
                  .Case<double>(() =>
                  {
                      double x = 0;
                      if (double.TryParse(att.Value, NumberStyles.Float, CultureInfo.CurrentCulture, out x))
                      {
                          value = (T)Convert.ChangeType(x, typeof(T));
                      }
                  })
                  .Case<float>(() =>
                  {
                      float x = 0;
                      if (float.TryParse(att.Value, NumberStyles.Float, CultureInfo.CurrentCulture, out x))
                      {
                          value = (T)Convert.ChangeType(x, typeof(T));
                      }
                  })
                  .Case<long>(() =>
                  {
                      long x = 0;
                      if (long.TryParse(att.Value, out x))
                      {
                          value = (T)Convert.ChangeType(x, typeof(T));
                      }
                  })
                  .Case<bool>(() =>
                  {
                      bool x = false;
                      if (bool.TryParse(att.Value, out x))
                      {
                          value = (T)Convert.ChangeType(x, typeof(T));
                      }
                  })
                  .Default<DateTime>(() =>
                  {
                      DateTime x = DateTime.MinValue;
                      if (DateTime.TryParse(att.Value, out x))
                      {
                          value = (T)Convert.ChangeType(x, typeof(T));
                      }
                  });
            }

            return value;
        }

        /// <summary>
        /// create the animation
        /// </summary>
        /// <param name="name">name of the Entity</param>
        /// <param name="fps">frames per second</param>
        /// <param name="scale">scale the animation</param>
        /// <param name="offset">offset the animation</param>
        /// <returns></returns>
        public Animator CreateAnimator(string name, float fps, Vector2 scale, Vector2 offset)
        {
            Entity entity = Entities.Where(s => string.Equals(s.Name, name, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

            if (entity == null)
            {
                throw new ArgumentNullException("name");
            }

            HandleMainLine(entity, fps, scale, offset);

            return new Animator(entity, _materials, fps);
        }

        private void HandleMainLine(Entity entity, float fps, Vector2 scale, Vector2 offset)
        {

            foreach (var anim in entity.Values)
            {
                //do all the tweening
                float frameTimeStep = 1000f / fps;
                float previousframeTime = 0;
                for (float frameTime = 0; frameTime <= anim.Length; frameTime += frameTimeStep)
                {
                    float currentTime = 0;
                    float nextTime = 0;

                    MainlineKey currentFrame = anim.GetCurrentMainlineKeyFromTime(frameTime);
                    MainlineKey nextFrame = anim.GetNextMainLineKeyFromTime(frameTime);

                    if (nextFrame == null)
                    {
                        //if looping then tweening is needed from the last to the first
                        nextFrame = anim.Loop ? anim.Mainline[0] : currentFrame;
                        nextTime = anim.Length;
                    }
                    else
                    {
                        nextTime = nextFrame.Time;
                    }

                    currentTime = frameTime - currentFrame.Time;
                    nextTime -= currentFrame.Time;

                    float timePct = MathHelper.Clamp(currentTime / nextTime, 0.0f, 1.0f);

                    //sprites
                    IList<SpriteFrame> frameImages = GetSpriteFrames(currentFrame, nextFrame, anim, timePct);

                    //bones
                    IList<BoneFrame> bones = GetBoneFrames(currentFrame, nextFrame, anim, timePct);

                    //boxes
                    IList<BoxFrame> boxes = GetBoxFrames(currentFrame, nextFrame, anim, timePct);

                    //points
                    IList<PointFrame> points = GetPointFrames(currentFrame, nextFrame, anim, timePct);

                    //create frame
                    Frame frame = new Frame
                    {
                        SpriteFrames = frameImages.OrderBy(s => s.ZOrder),
                        BoneFrames = bones,
                        BoxFrames = boxes,
                        PointFrames = points,
                        SoundEffectInstances = GetSoundEffects(frameTime, previousframeTime, anim)
                    };

                    //precalculate the transforms partially
                    foreach (var fimg in frame.SpriteFrames)
                    {
                        fimg.Transform = frame.ApplyBoneTransforms(fimg, scale, offset);
                    }

                    foreach (var box in frame.BoxFrames)
                    {
                        box.Transform = frame.ApplyTransform(new AnimationTransform(0, offset, scale, 1f), box.Scale, box.Angle, box.Position, box.Alpha);
                    }

                    foreach (var point in frame.PointFrames)
                    {
                        point.Transform = frame.ApplyTransform(new AnimationTransform(0, offset, scale, 1f), point.Scale, point.Angle, point.Position, point.Alpha);
                    }

                    anim.Frames.Add(frame);

                    previousframeTime = frameTime;
                }

            }
        }

        private IList<SoundEffectInstance> GetSoundEffects(float frameTime, float previousframeTime, Animation anim)
        {
            IList<SoundEffectInstance> sounds = new List<SoundEffectInstance>();
            SoundlineKey soundFrame = anim.GetCurrentSoundlineKeyFromTime(previousframeTime, frameTime);
            // SoundlineKey nextsoundFrame = anim.GetNextSoundLineKeyFromTime(frameTime);

            if (soundFrame != null)
            {
                foreach (var sound in soundFrame.KeyObjects)
                {
                    SoundEffect effect = _sounds[sound.FolderId][sound.FileId];
                    SoundEffectInstance instance = effect.CreateInstance();
                    instance.Volume = 1;
                    instance.Pitch = 0;
                    instance.Pan = 0;

                    sounds.Add(instance);
                }
            }

            return sounds;
        }

        private static IList<SpriteFrame> GetSpriteFrames(MainlineKey currentFrame, MainlineKey nextFrame, Animation anim, float timePct)
        {
            IList<SpriteFrame> frameImages = new List<SpriteFrame>();
            foreach (var currentKeyObject in currentFrame.KeyObjectRefs)
            {
                //get the reference from the next frame based on the TimeLine
                MainlineKeyObjectRef nextKeyObject = nextFrame.KeyObjectRefs.Where(x => x.TimeLine == currentKeyObject.TimeLine).FirstOrDefault();

                TimeLineKeySpriteObject next = nextKeyObject == null ? anim.TimeLines[currentKeyObject.TimeLine][currentKeyObject.Key] as TimeLineKeySpriteObject :
                    anim.TimeLines[nextKeyObject.TimeLine][nextKeyObject.Key] as TimeLineKeySpriteObject;

                if (next != null)
                {
                    TimeLineKeySpriteObject prev = anim.TimeLines[currentKeyObject.TimeLine][currentKeyObject.Key] as TimeLineKeySpriteObject;
                    SpriteFrame result = Tween<SpriteFrame>(prev.SpatialInfo, next.SpatialInfo, timePct, prev.Spin);

                    result.Folder = prev.MaterialFolderId;
                    result.File = prev.MaterialFileId;
                    result.Clockwise = prev.Spin;
                    result.ZOrder = currentKeyObject.ZOrder;
                    result.Parent = currentKeyObject.Parent;

                    frameImages.Add(result);
                }
            }

            return frameImages;
        }

        private static IList<BoneFrame> GetBoneFrames(MainlineKey currentFrame, MainlineKey nextFrame, Animation anim, float timePct)
        {
            List<BoneFrame> bones = new List<BoneFrame>();
            foreach (var currentBone in currentFrame.BoneObjectRefs)
            {
                MainLineKeyBoneObjectRef nextBone = nextFrame.BoneObjectRefs.Where(x => x.TimeLine == currentBone.TimeLine).FirstOrDefault();
                TimeLineKeyBoneObject next = nextBone == null ? anim.TimeLines[currentBone.TimeLine][currentBone.Key] as TimeLineKeyBoneObject :
                    anim.TimeLines[nextBone.TimeLine][nextBone.Key] as TimeLineKeyBoneObject;

                // Tweening
                TimeLineKeyBoneObject prev = anim.TimeLines[currentBone.TimeLine][currentBone.Key] as TimeLineKeyBoneObject;
                BoneFrame result = Tween<BoneFrame>(prev.SpatialInfo, next.SpatialInfo, timePct, prev.Spin);

                result.Parent = currentBone.Parent;

                bones.Add(result);
            }

            return bones;
        }

        private static IList<BoxFrame> GetBoxFrames(MainlineKey currentFrame, MainlineKey nextFrame, Animation anim, float timePct)
        {
            IList<BoxFrame> frameImages = new List<BoxFrame>();
            foreach (var currentKeyObject in currentFrame.KeyObjectRefs)
            {
                //get the reference from the next frame based on the TimeLine
                MainlineKeyObjectRef nextKeyObject = nextFrame.KeyObjectRefs.Where(x => x.TimeLine == currentKeyObject.TimeLine).FirstOrDefault();

                TimeLineKeyBoxObject next = nextKeyObject == null ? anim.TimeLines[currentKeyObject.TimeLine][currentKeyObject.Key] as TimeLineKeyBoxObject :
                    anim.TimeLines[nextKeyObject.TimeLine][nextKeyObject.Key] as TimeLineKeyBoxObject;

                if (next != null)
                {
                    TimeLineKeyBoxObject prev = anim.TimeLines[currentKeyObject.TimeLine][currentKeyObject.Key] as TimeLineKeyBoxObject;

                    BoxFrame result = Tween<BoxFrame>(prev.SpatialInfo, next.SpatialInfo, timePct, prev.Spin);

                    frameImages.Add(result);
                }
            }

            return frameImages;
        }

        private static IList<PointFrame> GetPointFrames(MainlineKey currentFrame, MainlineKey nextFrame, Animation anim, float timePct)
        {
            IList<PointFrame> frameImages = new List<PointFrame>();
            foreach (var currentKeyObject in currentFrame.KeyObjectRefs)
            {
                //get the reference from the next frame based on the TimeLine
                MainlineKeyObjectRef nextKeyObject = nextFrame.KeyObjectRefs.Where(x => x.TimeLine == currentKeyObject.TimeLine).FirstOrDefault();

                TimeLineKeyPointObject next = nextKeyObject == null ? anim.TimeLines[currentKeyObject.TimeLine][currentKeyObject.Key] as TimeLineKeyPointObject :
                    anim.TimeLines[nextKeyObject.TimeLine][nextKeyObject.Key] as TimeLineKeyPointObject;

                if (next != null)
                {
                    TimeLineKeyPointObject prev = anim.TimeLines[currentKeyObject.TimeLine][currentKeyObject.Key] as TimeLineKeyPointObject;
                    PointFrame result = Tween<PointFrame>(prev.SpatialInfo, next.SpatialInfo, timePct, prev.Spin);

                    frameImages.Add(result);
                }
            }

            return frameImages;
        }

        private static T Tween<T>(SpatialInfo prev, SpatialInfo next, float pct, bool clockWise) where T : FrameSpatialInfo, new()
        {
            T frame = (T)Activator.CreateInstance(typeof(T));
            frame.Pivot = Vector2.Lerp(prev.Pivot, next.Pivot, pct);
            frame.Scale = Vector2.Lerp(prev.Scale, next.Scale, pct);
            frame.Position = Vector2.Lerp(prev.Position, next.Position, pct);
            frame.Alpha = MathHelper.Lerp(prev.Alpha, next.Alpha, pct);

            float angleA = prev.Angle;
            float angleB = next.Angle;

            if (clockWise)
            {
                if (angleB - angleA < 0)
                {
                    angleB += MathHelper.TwoPi;
                }
                else
                {
                    angleA %= MathHelper.TwoPi;
                    angleB %= MathHelper.TwoPi;
                }
            }
            else
            {
                if (angleB - angleA > 0.0f)
                {
                    angleB -= MathHelper.TwoPi;
                }
                else
                {
                    angleA %= MathHelper.TwoPi;
                    angleB %= MathHelper.TwoPi;
                }
            }

            frame.Angle = MathHelper.Lerp(angleA, angleB, pct);

            return frame;
        }

        /// <summary>
        /// <entity> tags   
        /// </summary>
        internal IList<Entity> Entities { get; private set; }


    }
}
