﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Modular.Animation
{
    internal sealed class MaterialFolder : Dictionary<int, MaterialFile>
    {
        public MaterialFolder(XElement ele)
        {
            Id = AnimationData.GetValue<int>(ele.Attribute("id"));
            Name = AnimationData.GetValue<string>(ele.Attribute("name"));
            
            foreach (var row in ele.Elements("file"))
            {
                MaterialFile obj = null;
                string type = AnimationData.GetValue<string>(row.Attribute("type"));

                switch (type)
                {
                    case "sound":
                        obj = new MaterialSoundFile(row);
                        break;
                    default:
                        obj = new MaterialImageFile(row);
                        break;
                }

                this[obj.Id] = obj;
            }
        }


        public int Id { get; private set; }

        public string Name { get; private set; }
    }
}
