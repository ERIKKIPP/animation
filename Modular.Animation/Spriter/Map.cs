﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Modular.Animation
{
    internal sealed class Map
    {
        public Map(XElement ele)
        {
            FolderId = AnimationData.GetValue<int>(ele.Attribute("folder"));
            FileId = AnimationData.GetValue<int>(ele.Attribute("file"));
            TargetFolderId = AnimationData.GetValue<int>(ele.Attribute("target_folder"));
            TargetFileId = AnimationData.GetValue<int>(ele.Attribute("target_file"));
        }

        public int FolderId { get; private set; }

        public int FileId { get; private set; }

        public int TargetFolderId { get; private set; }

        public int TargetFileId { get; private set; }
    }
}
