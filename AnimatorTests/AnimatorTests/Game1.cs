using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Xml;
using System.Xml.Linq;
using Modular.Animation;
using System.Globalization;


namespace AnimatorTests
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        private const float FPS = 60;
        GraphicsDeviceManager graphics;
        SpriteBatch _spriteBatch;
        private List<Animator> _animators;
        private SpriteFont _font;
        private KeyboardState _lastKeyboard;
        private Texture2D _guideText;
        private int _frame;
        private int _currentAnim;
        private Rectangle _pointA;
        private Rectangle _pointB;
        private const int BackBufferWidth = 1280;
        private const int BackBufferHeight = 720;
        private int _characterMap;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = BackBufferWidth;
            graphics.PreferredBackBufferHeight = BackBufferHeight;
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            _animators = new List<Animator>();

            using (var reader = XmlReader.Create("TKaron.scml"))
            {
                XDocument doc = XDocument.Load(reader);
                AnimationData data = new AnimationData(doc, Content);

                Animator animator = data.CreateAnimator("Tkaron", FPS, Vector2.One, Vector2.Zero);
                animator.ChangeAnimation(_currentAnim);
                animator.Position = new Vector2(GraphicsDevice.Viewport.Width / 2, GraphicsDevice.Viewport.Height / 2);
                _animators.Add(animator);
            }

            using (var reader = XmlReader.Create("player.scml"))
            {
                XDocument doc = XDocument.Load(reader);
                AnimationData data = new AnimationData(doc, Content);

                Animator animator = data.CreateAnimator("Player", FPS, Vector2.One, Vector2.Zero);
                animator.ChangeAnimation(0);
                animator.Position = new Vector2(100, GraphicsDevice.Viewport.Height / 2);
                _animators.Add(animator);
            }

            using (var reader = XmlReader.Create("Mob01.scml"))
            {
                XDocument doc = XDocument.Load(reader);
                AnimationData data = new AnimationData(doc, Content);

                Animator animator = data.CreateAnimator("Mob01", FPS, Vector2.One, Vector2.Zero);
                animator.ChangeAnimation(0);
                animator.Position = new Vector2(250, GraphicsDevice.Viewport.Height);
                _animators.Add(animator);

                animator.ApplyCharacterMap(1);
            }

            using (var reader = XmlReader.Create("Ship.scml"))
            {
                XDocument doc = XDocument.Load(reader);
                AnimationData data = new AnimationData(doc, Content);

                Animator animator = data.CreateAnimator("Player", FPS, new Vector2(0.5f, 0.5f), Vector2.Zero);
                animator.ChangeAnimation(0);
                animator.Position = new Vector2(950, GraphicsDevice.Viewport.Height / 2);
                _animators.Add(animator);

                animator.ApplyCharacterMap(1);
            }

            _guideText = new Texture2D(GraphicsDevice, 1, 1, true, SurfaceFormat.Color);
            _guideText.SetData<Color>(new Color[] { Color.Black });
            _pointA = new Rectangle(0, GraphicsDevice.Viewport.Height / 2, GraphicsDevice.Viewport.Width, _guideText.Width);
            _pointB = new Rectangle(GraphicsDevice.Viewport.Width / 2, GraphicsDevice.Viewport.Height,
                GraphicsDevice.Viewport.Height, _guideText.Width);

            _font = Content.Load<SpriteFont>("SpriteFont1");

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        public bool IsKeyPress(KeyboardState current, KeyboardState previous, Keys key)
        {
            return current.IsKeyUp(key) && previous.IsKeyDown(key);
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            KeyboardState kb = Keyboard.GetState();

            if (IsKeyPress(kb, _lastKeyboard, Keys.Insert))
            {
                _characterMap++;
                foreach (var item in _animators)
                {
                    item.ApplyCharacterMap(_characterMap);
                }
            }

            if (IsKeyPress(kb, _lastKeyboard, Keys.Delete))
            {
                _characterMap--;
                foreach (var item in _animators)
                {
                    item.ApplyCharacterMap(_characterMap);
                }
            }

            // Advance frame: arrow/DPad left/right
            if (IsKeyPress(kb, _lastKeyboard, Keys.Left))
            {
                _frame--;
            }
            if (IsKeyPress(kb, _lastKeyboard, Keys.Right))
            {
                _frame++;
            }

            if (IsKeyPress(kb, _lastKeyboard, Keys.PageUp))
            {
                _currentAnim++;

                foreach (var item in _animators)
                {
                    item.ChangeAnimation(_currentAnim);
                }

            }
            if (IsKeyPress(kb, _lastKeyboard, Keys.PageDown))
            {
                _currentAnim--;
                foreach (var item in _animators)
                {
                    item.ChangeAnimation(_currentAnim);
                }
            }

            if (_frame == 0)
            {
                foreach (var item in _animators)
                {
                    item.Playing = true;
                    item.Update(gameTime);
                    item.PlaySound();
                }
            }
            else
            {
                foreach (var item in _animators)
                {
                    item.Playing = false;
                    item.SetFrame(_frame);
                    item.PlaySound();
                }
            }

            _lastKeyboard = kb;

            foreach (var item in _animators)
            {
                item.Update(gameTime);
                IList<Vector2> points = item.GetPoints();

            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin();

            foreach (var item in _animators)
            {
                item.Draw(_spriteBatch);
            }

            string debugInfo = string.Format("Frame: {0}", _frame);
            Vector2 position = new Vector2(0, GraphicsDevice.Viewport.TitleSafeArea.Bottom - _font.MeasureString(debugInfo).Y);
            _spriteBatch.DrawString(_font, debugInfo, position, Color.White);

            _spriteBatch.DrawString(_font, string.Format(CultureInfo.InvariantCulture, "Fps:{0}", 1 / (float)gameTime.ElapsedGameTime.TotalSeconds), Vector2.Zero, Color.Yellow);

            _spriteBatch.Draw(_guideText, _pointA, Color.White);
            _spriteBatch.Draw(_guideText, _pointB, null, Color.White, -(float)Math.PI / 2, Vector2.Zero, SpriteEffects.None, 1f);

            _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
